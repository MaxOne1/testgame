﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTime : MonoBehaviour
{
    public float TimeOfMove;

    private EnemyAutogun enemyGunScript;

    private Autogun playerGunScript;
    
    private Enemy enemyScript;
    
    private Player playerScript;
    
    private Jump jumpScript;
   
    public bool playerMove;
    public bool enemyMove;
    
    private Coroutine play;

 
    void Start()
    {
        
        if(playerGunScript != null)
            playerGunScript = GameObject.FindGameObjectWithTag("PlayerGun").GetComponent<Autogun>();
            
            enemyScript = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Enemy>();
       
            playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
       
            jumpScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Jump>();
       
            enemyGunScript = GameObject.FindGameObjectWithTag("Gun").GetComponent<EnemyAutogun>();

        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (play == null)
        {
            play = StartCoroutine(Move(TimeOfMove));
            
        }
      
        
       
    }
    IEnumerator Move(float moveTime)
    {
        PlayerMove();
        yield return new WaitForSeconds(moveTime);
        EnemyMove();
        yield return new WaitForSeconds(moveTime);
        play = null;
     
    }

       
    void PlayerMove()
    {
        playerMove = true;
        if (playerMove == true && playerScript!=null &&enemyScript != null)
        {
            enemyMove = false;
            enemyScript.agent.enabled = false;
            enemyGunScript.enabled = false;
            playerScript.enabled = true;
            playerScript.myAgent.enabled = true;
            enemyScript.agent.enabled = false;
            jumpScript.enabled = true;
            Debug.Log("Your turn");
        }
        
    }
    void EnemyMove()
    {
        enemyMove = true;
        if (enemyMove == true && enemyScript != null && playerScript != null)
        {
            playerMove = false;
            Debug.Log("Enemy turn");
            playerScript.myAgent.enabled = false;
            jumpScript.enabled = false;
            enemyScript.enabled = true;
            enemyGunScript.enabled = true;
            enemyGunScript.bulletCount = 10;
        }
    }

 }
