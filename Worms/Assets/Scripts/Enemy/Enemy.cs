﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Transform player;
    
    private Vector3 originPos;
   
    public float distance;
    public float speed;
    
    public LayerMask click;
    
    public NavMeshAgent agent;
    
    public int HP = 100;
    
    private MoveTime timeScript;
    
    private Player playerScript;
    
    private Animator anim;

    public bool isAlive;

    void Start()
    {
        timeScript = GameObject.Find("MoveTime").GetComponent<MoveTime>();
        
        agent = GetComponent<NavMeshAgent>();
        
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        
        anim = GetComponent<Animator>();

        isAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        Distance();
        Moving();
        Death();
    }
    void  Death()
    {
        if (HP <= 0)
        {
            isAlive = false;
            anim.SetBool("Dead", true);
            Destroy(gameObject, 5f);
        }
    }
    void Moving()
    {
        if (distance > 2 && timeScript.enemyMove == true && playerScript.isAlive == true && isAlive == true)
        {
            agent.enabled = true;
            agent.destination = player.position;
        }
        if (distance <= 2)
        {
            agent.enabled = false;
        }
    }
    void Distance()
    {
        originPos = transform.position;
        if (player != null)
            distance = (player.position - originPos).magnitude;
    }
}
