﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Jump : MonoBehaviour
{
    private float firstClick, timeBetweenClicks;
    private int clickCount;
    private bool coroutine;

    public float jumpForce;
    public float gravityMod;
    private Rigidbody rb;
    private Player playerScript;
    public bool isGround;
    public Animator anim;
    void Start()
    {
        firstClick = 0f;
        clickCount = 0;
        timeBetweenClicks = 0.2f;
        coroutine = true;

        anim = GetComponent<Animator>();
        anim.SetBool("IsGround", true);

        rb = GetComponent<Rigidbody>();
        Physics.gravity *= gravityMod;
        
            playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        
     
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clickCount += 1;
        }
        if(clickCount ==1 && coroutine)
        {
            firstClick = Time.time;
            StartCoroutine(DoubleClickOn());
        }

        


    }
    private IEnumerator DoubleClickOn()
    {
        coroutine = false;
        while(Time.time < firstClick + timeBetweenClicks)
        {
            if(clickCount == 2 && isGround == true)
            {
                Debug.Log("Double");
                playerScript.myAgent.enabled = false;
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGround = false;
                anim.SetBool("IsGround", false);
                break;

            }
            else
            {
                anim.SetBool("IsGround", true);
            }
            yield return new WaitForEndOfFrame();
        }
        clickCount = 0;
        firstClick = 0f;
        coroutine = true;
  
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground") && playerScript != null)
        {
            isGround = true;
            playerScript.myAgent.enabled = true;
        }

        
    }

}
