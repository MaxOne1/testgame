﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autogun : MonoBehaviour
{
    public Transform muzzle;
    
    public int damage = 3;
    public int bulletCount = 10;

    private float nextFire = 0f;
    public float fireRate;
    
    public AudioClip shoot;
    private AudioSource shootAudio;
    
    private Enemy enemyScript;
    
    private MoveTime timeScript;
    
    private Player playerScript;

    void Start()
    {
        shootAudio = GetComponent<AudioSource>();
        
        enemyScript = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Enemy>();
        
        timeScript = GameObject.Find("MoveTime").GetComponent<MoveTime>();
        
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire && Input.GetMouseButton(0) && timeScript.enemyMove == false && playerScript.isAlive == true)
        {

            nextFire = Time.time  + 1f/ fireRate;
            if(bulletCount != 0)
            Shoot();
            
        }
        if(timeScript.playerMove == false)
            bulletCount = 10;
       
        
    }

    void Shoot()
    {
        Ray ray = new Ray(muzzle.position, transform.forward);
        RaycastHit hit;
        
        Debug.DrawRay(transform.position, ray.direction * 100, Color.red);
        shootAudio.PlayOneShot(shoot, 1f);
        bulletCount--;
        


        if (Physics.Raycast(ray, out hit, 100))
        {
            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.tag == "Enemy" && enemyScript != null)
            {
                enemyScript.HP -= damage;
            }
        }
    }
}
