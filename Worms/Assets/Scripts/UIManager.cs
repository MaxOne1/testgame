﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI lose;
    private Player playerScript;
    public Button reclame;
    void Start()
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        lose.gameObject.SetActive(false);
        reclame.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(playerScript.isAlive == false)
        {
            lose.gameObject.SetActive(true);
        }
        else
        {
            lose.gameObject.SetActive(false);
        }
        if(playerScript.isAlive == false)
        {
            reclame.gameObject.SetActive(true);
        }
        else
        {
            reclame.gameObject.SetActive(false);
        }
    }
    
    
}
